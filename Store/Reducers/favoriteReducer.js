const initialState = { favoritesFilm: [] }

function toggleFavorite(state = initialState, action) {
	let nextSate
	switch(action.type) {
		case 'TOGGLE_FAVORITE':
			const favoriteFilmIndex = state.favoritesFilm.findIndex(item => item.id === action.value.id)
			if (favoriteFilmIndex !== -1) {
				nextSate = {
					...state,
					favoritesFilm: state.favoritesFilm.filter( (item, index) => index !== favoriteFilmIndex )
				}
			} else {
				nextSate = {
					...state,
					favoritesFilm: [ ...state.favoritesFilm, action.value ]
				}
			}
			return nextSate || state
		default:
			return state
	}
}

export default toggleFavorite